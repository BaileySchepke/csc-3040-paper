\section{Offline Data Brokers}

While Facebook captures much of its own data through browsing on its platforms and advertising services, this pertains largely to online activities and online social engagements.
In order to obtain more information to fine-grain its advertising services, Facebook partnered with the traditionally-offline data brokers Acxiom, Experian, Epsilon, and Oracle Data Cloud.
These data brokers operate by harvesting information from “voter records, vehicle registries, loyalty cards, and so forth” and their business model largely was catering to traditional marketing and political campaigning. \cite{venkatadri2019}
However, linking these traditionally offline data sets to Facebook’s database of online information enabled “online services to provide advertisers with targeting features that concern users’ offline information”. \cite{venkatadri2019}
Using this combined information, Facebook provided advertisers with “partner category” attributes with which to define audiences, groups of users who share particular attributes or sets of attributes.
These attributes include country, state, postal code, age range, gender and “a list containing 1121 [more] attributes”. \cite{venkatadri2019}
These sets were available for the U.S., Japan, Australia, the U.K., Germany, France, and Brazil. The researchers manipulated the advertising interface to discover that upwards of 90\% of Facebook accounts for users in the United States had been successfully linked to information sets from these offline data brokers.
Not only this, but also the Ad Preferences page for Facebook account holders only disclosed information that Facebook had learned or inferred about users and had no mention of their possession of the offline data broker data sets.
In October of 2020, Facebook removed these categories as part of its response to the events involving Cambridge Analytica.
It is unknown whether or not this information was retained for Facebook’s internal use.

To analyze these actions from a Utilitarian perspective, under which the moral action is the one that leads to the greatest amount of happiness for the greatest number of people, one would have to assign values to the happiness of the workers at Facebook, the data brokers, and their shareholders.
One would then need to estimate the number of people affected either by the direct data collection or the targeted advertisements generated using that collected data.
It would have to be taken into account how many of these advertisements were helpful to the targets and how many were a nuisance.
It would also have to be considered the repercussions of incorrect data used in these contexts.
For instance, if a user is incorrectly identified as a consumer of sexually explicit media and is subsequently served advertisements that reflect that tagging, that has the potential to damage that individual’s reputation depending on where those show up and in what contexts.
I do not have concrete data for these proposed metrics, but a cursory view of the problem would lead one to believe that the actions of Facebook in supplying advertisers with all of this information has caused more harm than it has good.
By the nature of advertising, there will nearly always be more targets of the advertisement being potentially harmed by it, even if it is only a light harm such as annoyance, than there will be advertisers who benefit from serving these.
Under such an analysis, one would conclude that Facebook’s use of this combined data for its advertising platform is unethical.

If one instead considers Facebook’s combination with and utilization of the data sets provided by the traditionally-offline data brokers through the lens of Social Contract Theory, it may be possible to better justify their actions.
Facebook provides many services that users enjoy for free, such as its Facebook social platform, Messenger messaging application, the Instagram photo sharing application, and many SDKs and APIs used widely across the Internet.
While these do not cost money to use, Facebook benefits from their use by collecting any information they can that flows through them.
This is the implicit social contract of using Facebook services. Rather than paying with money, users pay with information about themselves.
A well-informed user will know this and make the decision to accept the social contract and use the services or will reject this social contract and refrain from using them. Under this, one might view Facebook’s actions as ethical.
However, the addition of the offline data brokers may change this. There is nothing in the implicit social contract that could be construed to grant Facebook the privilege of connecting the collected online data set of an individual with offline information that the user is unaware of.
Whether this is ethical or not depends on one’s view of the ownership of one’s information after it has been “traded” for use of the online services that Facebook provides.
If one thinks that Facebook earned that information through a fair trade and now owns it and may do with it as it pleases, then this is still ethical behavior. If one believes that individuals should retain some control of their information post-trade, then this is not ethical.
The user did not consent to the sale of their information by the offline data brokers, nor did they consent to its combination with their online data.
Regardless of which view one takes, it was unethical for Facebook to claim transparency by providing an Ad Preferences page and hiding the fact that it did indeed possess this information set from another source.
This was very misleading on their part and damaged the trust that users had in this supposed transparency as well as the company’s transparency statement overall.
Because of this, one should still conclude that Facebook’s actions were unethical even under the Social Contract Theory.

Among the attributes provided in the advertising platform was a category called “multicultural affinity” which was a reference to “a user’s affinity with various racial and ethnic groups." \cite{hitlin2019}
This was not a reflection of the user’s own race but was instead intended to reflect interest in African American culture, Hispanic culture, or Asian American culture.
In July of 2018, the Washington State Attorney had Facebook sign an agreement stating that it “would no longer let advertisers unlawfully exclude users by race, religion, sexual orientation, and other protected classes." \cite{hitlin2019}
This had the potential for advertisers to target individuals for discriminatory or divisive purposes.
One speculated abuse of this would be targeted political campaign advertisements based on racial concerns.
Along with potentially dangerous attributes were found some exploits that endangered the privacy of individuals.
In a study led by Irfan Faizullabhoy and Aleksandra Korolova that examined exploits within the advertising platform, it was found that the Audience Insights feature, which is used to list attributes that are common among a group of targets, “can be run on audiences as small as one person, and when run, insights include 2,000+ categories of information.” \cite{faizullabhoy2018}
Not only this, but also the targeting by location feature allowed for “Facebook-approved ad campaigns that target[ed] arbitrarily small locations (as small as a single house)." \cite{faizullabhoy2018}
Using these, the researchers were able to divulge a great deal of information about their friends who willingly participated in the study.
They reported all of their findings to Facebook’s Responsible Disclosure Program, but only the Single-Person Insight attack was addressed.
The researchers note that the only white hat testing allowed for the platform used test user accounts that have no data associated with them and that, had they been adhering to that, it would not have been possible to produce any meaningful research.
They also note that the current set of exploits make “stalking and harassment easy and cheap” for “only a few cents.” \cite{faizullabhoy2018}

Under the Social Contract theory, Facebook has the right to collect the information that they want, provided they do so responsibly and do not put their users at risk with their advertising platform.
They failed to protect their users by allowing for their platform to promote various types of discrimination with their multicultural affinity attribute and they also failed to properly protect the data they had collected by leaving open these single-user ad campaign attacks.
This leaves the user base in danger of having potentially sensitive information compromised in ways that they could never anticipate.
This is a poor design that falls outside the IEEE code of ethics in that it fails to uphold the safety of users and fails to inform them of the danger, not to mention their utter disregard for the risk.
They failed to produce a quality service and by ignoring the problem they are consciously risking damages to their user base.